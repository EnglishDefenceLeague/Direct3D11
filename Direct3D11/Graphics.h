#ifndef _GRAPHICS_H_
#define _GRAPHICS_H_

#include "Engine.h"

class Graphics
{
private:
	Engine* DirectEngine;
public:
	Graphics(LONG ScreenWidth,
		LONG ScreenHeight,
		BOOLEAN VSync,
		HWND WindowHandle,
		BOOLEAN FullScreen,
		FLOAT ScreenDepth,
		FLOAT ScreenNear,
		BOOLEAN* Status);
	~Graphics();
	BOOLEAN BeginScene(
		FLOAT Red, 
		FLOAT Green,
		FLOAT Blue,
		FLOAT Alpha);
	BOOLEAN EndScene();
	VOID ClearScreen(
		FLOAT Red,
		FLOAT Green,
		FLOAT Blue,
		FLOAT Alpha);
};
#endif