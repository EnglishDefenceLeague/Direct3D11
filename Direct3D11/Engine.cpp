#include "Engine.h"


Engine::Engine()
{
	VideoCardMemory = 0;

	RtlSecureZeroMemory(
		VideoCardDescrption,
		sizeof(VideoCardDescrption));

	SwapChain = NULL;
	Device = NULL;
	DeviceContext = NULL;
	RenderTargetView = NULL;
	DepthStencilBuffer = NULL;
	DepthStencilState = NULL;
	DepthStencilView = NULL;
	RasterizeState = NULL;
	VSync = FALSE;
}

Engine::~Engine()
{
	if (SwapChain)
	{
		SwapChain->SetFullscreenState(FALSE, NULL);

	}
	if (RasterizeState)
	{
		RasterizeState->Release();
		RasterizeState = NULL;
	}
	if (DepthStencilView)
	{
		DepthStencilView->Release();
		DepthStencilView = NULL;
	}
	if (DepthStencilState)
	{
		DepthStencilState->Release();
		DepthStencilState = NULL;
	}

	if (DepthStencilBuffer)
	{
		DepthStencilBuffer->Release();
		DepthStencilBuffer = NULL;
	}

	if (SwapChain)
	{
		SwapChain->Release();
		SwapChain = NULL;
	}

	if (Device)
	{
		Device->Release();
		Device = NULL;
	}

	if (DeviceContext)
	{
		DeviceContext->Release();
		DeviceContext = NULL;
	}
}

BOOLEAN Engine::Initialize(LONG ScreenWidth, 
						   LONG ScreenHeight, 
						   BOOLEAN VSync,
						   HWND WindowHandle,
						   BOOLEAN FullScreen,
						   FLOAT ScreenDepth,
						   FLOAT ScreenNear)
{
	HRESULT Result = S_FALSE;

	IDXGIFactory* Factory = NULL;
	IDXGIAdapter* Adapter = NULL;
	IDXGIOutput* AdapterOutput = NULL;
	DXGI_MODE_DESC* DisplayModeList = NULL;
	ID3D11Texture2D* BackBuffer = NULL;


	DXGI_ADAPTER_DESC AdapterDescrption = { 0 };
	DXGI_SWAP_CHAIN_DESC SwapChainDescription = { 0 };
	D3D11_TEXTURE2D_DESC DepthBufferDescription = { 0 };
	D3D11_DEPTH_STENCIL_DESC DepthStencilDescriptionBuffer = { 0 };
	D3D11_RASTERIZER_DESC RasterizerDescription;
	D3D11_DEPTH_STENCIL_VIEW_DESC DepthStencilViewDescriptionBuffer;
	D3D11_VIEWPORT ViewPort = { 0 };


	D3D_FEATURE_LEVEL FeatureLevel = D3D_FEATURE_LEVEL_11_0;

	UINT DisplayModes = 0;
	UINT Mode = 0;
	LONG RefreshRateNumberator = 0;
	LONG RefreshRateDenominator = 0;

	FLOAT FieldOfView = 0.0f;
	FLOAT ScreenAspect = 0.0f;

	this->VSync = VSync;
	Result = CreateDXGIFactory(
		__uuidof(IDXGIFactory),
		(LPVOID*)&Factory);
	if (FAILED(Result))
		return FALSE;

	Result = Factory->EnumAdapters(
		0,
		&Adapter);

	if (FAILED(Result))
	{
		Factory->Release();
		return FALSE;

	}

	Result = Adapter->EnumOutputs(
		0,
		&AdapterOutput);
	if (FAILED(Result))
	{
		Adapter->Release();
		Factory->Release();
		return FALSE;
	}

	Result = AdapterOutput->GetDisplayModeList(
		DXGI_FORMAT_R8G8B8A8_UNORM,
		DXGI_ENUM_MODES_INTERLACED,
		&DisplayModes,
		NULL);
	if (FAILED(Result))
	{
		AdapterOutput->Release();
		Adapter->Release();
		Factory->Release();
		return FALSE;
	}

	DisplayModeList = new DXGI_MODE_DESC[DisplayModes];
	if (!DisplayModeList)
	{
		AdapterOutput->Release();
		Adapter->Release();
		Factory->Release();
		return FALSE;
	}
	Result = AdapterOutput->GetDisplayModeList(
		DXGI_FORMAT_R8G8B8A8_UNORM,
		DXGI_ENUM_MODES_INTERLACED,
		&DisplayModes,
		DisplayModeList);
	if (FAILED(Result))
	{
		delete[] DisplayModeList;
		AdapterOutput->Release();
		Adapter->Release();
		Factory->Release();
		return FALSE;
	}

	for (Mode = 0; Mode < DisplayModes; ++Mode)
	{
		if (DisplayModeList[Mode].Width == ScreenWidth)
		{
			if (DisplayModeList[Mode].Height == ScreenHeight)
			{
				RefreshRateNumberator = DisplayModeList[Mode].RefreshRate.Numerator;
				RefreshRateDenominator = DisplayModeList[Mode].RefreshRate.Denominator;
			}
		}
	}

	Result = Adapter->GetDesc(&AdapterDescrption);
	if (FAILED(Result))
	{
		delete[] DisplayModeList;
		AdapterOutput->Release();
		Adapter->Release();
		Factory->Release();
		return FALSE;
	}

	VideoCardMemory = AdapterDescrption.DedicatedVideoMemory / 1024 / 1024;
	memcpy(VideoCardDescrption, AdapterDescrption.Description, sizeof(VideoCardDescrption));

	delete[] DisplayModeList;
	AdapterOutput->Release();
	Adapter->Release();
	Factory->Release();

	SwapChainDescription.BufferCount = 1;
	SwapChainDescription.BufferDesc.Width = ScreenWidth;
	SwapChainDescription.BufferDesc.Height = ScreenHeight;
	SwapChainDescription.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	if (VSync)
	{
		SwapChainDescription.BufferDesc.RefreshRate.Numerator = RefreshRateNumberator;
		SwapChainDescription.BufferDesc.RefreshRate.Denominator = RefreshRateDenominator;
	}
	else
	{
		SwapChainDescription.BufferDesc.RefreshRate.Numerator = 0;
		SwapChainDescription.BufferDesc.RefreshRate.Denominator = 1;
	}

	SwapChainDescription.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	SwapChainDescription.OutputWindow = WindowHandle;
	SwapChainDescription.SampleDesc.Count = 1;
	SwapChainDescription.SampleDesc.Quality = 0;

	if (!FullScreen)
		SwapChainDescription.Windowed = TRUE;

	SwapChainDescription.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	SwapChainDescription.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	SwapChainDescription.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

	Result = D3D11CreateDeviceAndSwapChain(
		NULL,
		D3D_DRIVER_TYPE_HARDWARE,
		NULL,
		0,
		&FeatureLevel,
		1,
		D3D11_SDK_VERSION,
		&SwapChainDescription,
		&SwapChain,
		&Device,
		NULL,
		&DeviceContext);
	if (FAILED(Result))
		return FALSE;

	Result = SwapChain->GetBuffer(
		0,
		__uuidof(ID3D11Texture2D),
		(LPVOID*)&BackBuffer);
	if (FAILED(Result))
		return FALSE;

	Result = Device->CreateRenderTargetView(
		BackBuffer,
		NULL,
		&RenderTargetView);

	BackBuffer->Release();

	if (FAILED(Result))
		return FALSE;

	DepthBufferDescription.Width = ScreenWidth;
	DepthBufferDescription.Height = ScreenHeight;
	DepthBufferDescription.MipLevels = 1;
	DepthBufferDescription.ArraySize = 1;
	DepthBufferDescription.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	DepthBufferDescription.SampleDesc.Count = 1;
	DepthBufferDescription.SampleDesc.Quality = 0;
	DepthBufferDescription.Usage = D3D11_USAGE_DEFAULT;
	DepthBufferDescription.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	DepthBufferDescription.CPUAccessFlags = 0;
	DepthBufferDescription.MiscFlags = 0;

	Result = Device->CreateTexture2D(
		&DepthBufferDescription,
		NULL,
		&DepthStencilBuffer);
	if (FAILED(Result))
		return FALSE;

	DepthStencilDescriptionBuffer.DepthEnable = TRUE;
	DepthStencilDescriptionBuffer.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	DepthStencilDescriptionBuffer.DepthFunc = D3D11_COMPARISON_LESS;

	DepthStencilDescriptionBuffer.StencilEnable = TRUE;
	DepthStencilDescriptionBuffer.StencilReadMask = 0xFF;
	DepthStencilDescriptionBuffer.StencilWriteMask = 0xFF;

	DepthStencilDescriptionBuffer.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	DepthStencilDescriptionBuffer.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	DepthStencilDescriptionBuffer.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	DepthStencilDescriptionBuffer.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;

	DepthStencilDescriptionBuffer.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	DepthStencilDescriptionBuffer.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	DepthStencilDescriptionBuffer.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	DepthStencilDescriptionBuffer.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;

	Result = Device->CreateDepthStencilState(
		&DepthStencilDescriptionBuffer,
		&DepthStencilState);
	if (FAILED(Result))
		return FALSE;

	DeviceContext->OMSetDepthStencilState(
		DepthStencilState,
		1);

	RtlSecureZeroMemory(
		&DepthStencilViewDescriptionBuffer,
		sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
	DepthStencilViewDescriptionBuffer.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	DepthStencilViewDescriptionBuffer.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	DepthStencilViewDescriptionBuffer.Texture2D.MipSlice = 0;

	Result = Device->CreateDepthStencilView(
		DepthStencilBuffer,
		&DepthStencilViewDescriptionBuffer,
		&DepthStencilView);
	if (FAILED(Result))
		return FALSE;

	DeviceContext->OMSetRenderTargets(
		1,
		&RenderTargetView,
		DepthStencilView);

	RtlSecureZeroMemory(
		&RasterizerDescription,
		sizeof(D3D11_RASTERIZER_DESC));

	RasterizerDescription.AntialiasedLineEnable = FALSE;
	RasterizerDescription.CullMode = D3D11_CULL_BACK;
	RasterizerDescription.DepthBias = 0;
	RasterizerDescription.DepthBiasClamp = 0.0f;
	RasterizerDescription.DepthClipEnable = TRUE;
	RasterizerDescription.FillMode = D3D11_FILL_SOLID;
	RasterizerDescription.FrontCounterClockwise = FALSE;
	RasterizerDescription.MultisampleEnable = FALSE;
	RasterizerDescription.ScissorEnable = FALSE;
	RasterizerDescription.SlopeScaledDepthBias = 0.0f;

	Result = Device->CreateRasterizerState(
		&RasterizerDescription,
		&RasterizeState);
	if (FAILED(Result))
	{
		return FALSE;
	}

	DeviceContext->RSSetState(RasterizeState);

	ViewPort.Width = (FLOAT)ScreenWidth;
	ViewPort.Height = (FLOAT)ScreenHeight;
	ViewPort.MinDepth = 0.0f;
	ViewPort.MaxDepth = 1.0f;
	ViewPort.TopLeftX = 0.0f;
	ViewPort.TopLeftY = 0.0f;

	DeviceContext->RSSetViewports(
		1,
		&ViewPort);

	Projection = XMMatrixPerspectiveFovLH
		(
			FieldOfView,
			ScreenAspect,
			ScreenNear,
			ScreenDepth
		);

	World = XMMatrixIdentity();
	
	Ortho = XMMatrixOrthographicLH
		(
			(FLOAT)ScreenWidth,
			(FLOAT)ScreenHeight,
			ScreenNear,
			ScreenDepth
		);
	return TRUE;

}

ID3D11Device * Engine::GetDevice()
{
	return Device;
}

ID3D11DeviceContext * Engine::GetDeviceContext()
{
	return DeviceContext;
}

BOOLEAN Engine::BeginScene(FLOAT Red,
						   FLOAT Green,
						   FLOAT Blue,
						   FLOAT Alpha)
{
	FLOAT RenderColour[] = 
	{ 
		Red, 
		Green, 
		Blue, 
		Alpha 
	};

	DeviceContext->ClearRenderTargetView(
		RenderTargetView,
		RenderColour);

	DeviceContext->ClearDepthStencilView(
		DepthStencilView,
		D3D11_CLEAR_DEPTH,
		1.0f,
		0);

	return  TRUE;
}

BOOLEAN Engine::EndScene()
{
	if (VSync)
		SwapChain->Present(1, 0);
	else
		SwapChain->Present(0, 0);
	return TRUE;
}

VOID Engine::ClearScreen(FLOAT Red,
						 FLOAT Green,
						 FLOAT Blue,
						 FLOAT Alpha)
{
	FLOAT ScreenColour[] = 
	{
		Red,
		Green,
		Blue,
		Alpha
	};
	DeviceContext->ClearRenderTargetView(RenderTargetView,ScreenColour);
}

