#include "Graphics.h"

LRESULT CALLBACK WindowsCallbackProcedure(HWND WindowHandle,
										  UINT Message,
										  WPARAM WideParam,
										  LPARAM LongParam);
INT MessageLoop(LPRECT WindowDimensions,
				HWND WindowHandle);
#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 1024
INT WINAPI WinMain(HINSTANCE CurrentInstance,
				   HINSTANCE OldInstance,
				   PCHAR CmdLine,
				   INT CmdShow)
{
	WNDCLASSEXW WindowClass = { 0 };
	RECT WindowDimensions = { 0 };

	HWND WindowHandle = NULL;
	INT Message = 0;

	WindowClass.cbSize = sizeof(WNDCLASSEXW);
	WindowClass.lpszClassName = L"WindowClass";
	WindowClass.hInstance = CurrentInstance;
	WindowClass.hbrBackground = (HBRUSH)COLOR_WINDOW;
	WindowClass.style = CS_HREDRAW | CS_VREDRAW;
	WindowClass.hCursor = LoadCursorW(NULL, IDC_ARROW);
	WindowClass.lpfnWndProc = WindowsCallbackProcedure;


	RegisterClassExW(&WindowClass);

	WindowDimensions.right = SCREEN_WIDTH;
	WindowDimensions.bottom = SCREEN_HEIGHT;
	
	WindowHandle = CreateWindowExW(WS_EX_OVERLAPPEDWINDOW, 
		L"WindowClass",
		L"Window",
		WS_OVERLAPPEDWINDOW | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX,
		CW_USEDEFAULT,CW_USEDEFAULT,
		WindowDimensions.right - WindowDimensions.left,
		WindowDimensions.bottom - WindowDimensions.top,
		NULL,
		NULL,
		NULL,
		NULL);

	if (!WindowHandle)
		return FALSE;

	ShowWindow(WindowHandle,
		CmdShow);

	Message = MessageLoop(&WindowDimensions,
		WindowHandle);

	UnregisterClassW(L"WindowClass",
		CurrentInstance);

	DestroyWindow(WindowHandle);



	return Message;
}
#define SCREEN_DEPTH 1000.0f
#define SCREEN_NEAR 0.1f
BOOLEAN BeginScene(Graphics* GraphicsEngine)
{
	return 
		GraphicsEngine->BeginScene
		(
			0.5f,
			0.5f,
			0.5f,
			1.0f
		);
}
VOID ClearScreen(Graphics* GraphicsEngine)
{
	GraphicsEngine->ClearScreen
		(
			0.0f,
			0.0f,
			0.0f,
			0.0f
		);
}
BOOLEAN EndScene(Graphics* GraphicsEngine)
{
	return GraphicsEngine->EndScene();
}
BOOLEAN RenderFrame(Graphics* GraphicsEngine)
{
	BOOLEAN Status = TRUE;
	
	Status = BeginScene(GraphicsEngine);

	ClearScreen(GraphicsEngine);

	Status &= GraphicsEngine->EndScene();

	return Status;
}
INT MessageLoop(LPRECT WindowDimensions,
				HWND WindowHandle)
{
	MSG	Message = { 0 };
	Graphics* GraphicsEngine = NULL;
	BOOLEAN Status = FALSE;

	GraphicsEngine = new Graphics(
		WindowDimensions->right - WindowDimensions->left,
		WindowDimensions->bottom - WindowDimensions->top,
		TRUE,
		WindowHandle,
		FALSE,
		SCREEN_DEPTH,
		SCREEN_NEAR,
		&Status);

	if (!Status)
	{
		delete GraphicsEngine;
		GraphicsEngine = NULL;
		return NULL;
	}
	while (Message.message != WM_QUIT)
	{

		if (PeekMessageW(
			&Message,
			NULL,
			0,
			0,
			PM_REMOVE))
		{
			TranslateMessage(&Message);
			DispatchMessageW(&Message);
			continue;
		}

		RenderFrame(GraphicsEngine);
	}

	delete GraphicsEngine;
	GraphicsEngine = NULL;

	return WM_QUIT;
}

BOOLEAN CursorIsHidden = FALSE;

LRESULT CALLBACK WindowsCallbackProcedure(HWND WindowHandle,
										  UINT Message,
										  WPARAM WideParam,
										  LPARAM LongParam)
{
	switch (Message)
	{
	case WM_DESTROY :
		PostQuitMessage(0);
		break;
	case WM_SETCURSOR:
		WORD HitTestCode = LOWORD(LongParam);
		switch(HitTestCode)
		{
		case HTCLIENT:
			if(!CursorIsHidden)
			{
				CursorIsHidden = TRUE;
				ShowCursor(FALSE);
			}
			break;
		default:
			if(CursorIsHidden)
			{
				CursorIsHidden = FALSE;
				ShowCursor(TRUE);
			}
		}
		break;
	}
	return DefWindowProcW(
		WindowHandle,
		Message,
		WideParam,
		LongParam);
}