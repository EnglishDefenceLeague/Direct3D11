#ifndef _ENGINE_H_
#define _ENGINE_H_

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <d3d11.h>
#include <DirectXMath.h>

#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3d11.lib")
using namespace DirectX;

class Engine
{
private:
	SIZE_T VideoCardMemory;
	BYTE VideoCardDescrption[128];
	IDXGISwapChain* SwapChain;
	ID3D11Device* Device;
	ID3D11DeviceContext* DeviceContext;
	ID3D11RenderTargetView* RenderTargetView;
	ID3D11Texture2D* DepthStencilBuffer;
	ID3D11DepthStencilState* DepthStencilState;
	ID3D11DepthStencilView* DepthStencilView;
	ID3D11RasterizerState* RasterizeState;
	BOOLEAN VSync;
	XMMATRIX Projection;
	XMMATRIX World;
	XMMATRIX Ortho;
public:

	Engine::Engine();
	Engine::~Engine();

	BOOLEAN Initialize(
		LONG ScreenWidth, 
		LONG ScreenHeight, 
		BOOLEAN VSync,
		HWND WindowHandle,
		BOOLEAN FullScreen,
		FLOAT ScreenDepth,
		FLOAT ScreenNear);

	ID3D11Device* GetDevice();
	ID3D11DeviceContext* GetDeviceContext();
	BOOLEAN  BeginScene(
		FLOAT Red,
		FLOAT Green,
		FLOAT Blue,
		FLOAT Alpha);
	BOOLEAN  EndScene();
	VOID ClearScreen(
		FLOAT Red,
		FLOAT Green,
		FLOAT Blue,
		FLOAT Alpha);
};
#endif