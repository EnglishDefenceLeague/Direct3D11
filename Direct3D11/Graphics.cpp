#include "Graphics.h"
Graphics::Graphics(LONG ScreenWidth,
				   LONG ScreenHeight,
				   BOOLEAN VSync,
				   HWND WindowHandle,
				   BOOLEAN FullScreen,
				   FLOAT ScreenDepth,
				   FLOAT ScreenNear,
				   BOOLEAN* Status)
{
	if (Status)
	{
		DirectEngine = new Engine();
		if (DirectEngine->Initialize(ScreenWidth, ScreenHeight, VSync, WindowHandle, FullScreen, ScreenDepth, ScreenNear))
			*Status = TRUE;
		else
		{
			*Status = FALSE;
			delete DirectEngine;
			DirectEngine = NULL;
		}
	}
}
Graphics::~Graphics()
{
	if (DirectEngine)
	{
		delete DirectEngine;
		DirectEngine = NULL;
	}
}

BOOLEAN Graphics::BeginScene(FLOAT Red,
							 FLOAT Green,
							 FLOAT Blue,
							 FLOAT Alpha)
{
	return DirectEngine->BeginScene(Red, Green, Blue, Alpha);
}

BOOLEAN Graphics::EndScene()
{
	return DirectEngine->EndScene();
}

VOID Graphics::ClearScreen(
	FLOAT Red,
	FLOAT Green,
	FLOAT Blue,
	FLOAT Alpha)
{
	DirectEngine->ClearScreen(
		Red,
		Green,
		Blue,
		Alpha);
}
